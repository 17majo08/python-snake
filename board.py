# Using turtle library for graphics
#
# @author Max Jonsson
# @version 11/10-2019

import turtle
import random


def setup_screen(w: int, h: int) -> turtle.Screen:
    window = turtle.Screen()
    window.setup(width=w, height=h)
    window.bgcolor("light green")
    window.title("Snake - The game")

    return window


def draw_score_text(x: int, y: int, score: int, highscore: int) -> turtle:
    pen = turtle.Turtle()
    pen.penup()
    pen.hideturtle()
    pen.goto(x, y)

    update_score_text(pen, score, highscore)

    return pen


def update_score_text(pen: turtle, score: int, highscore: int):
    pen.clear()
    pen.write(f"Score: {score}  High Score: {highscore}",
              align="center", font=("Courier", 24, "normal"))


def draw_snake(x: int, y: int) -> turtle:
    head = turtle.Turtle()
    head.shape('square')
    head.color('black')
    head.penup()
    head.goto(x, y)
    head.direction = 'stop'

    return head


def span_food(x_min: int, x_max: int, y_min: int, y_max: int) -> turtle:
    food = turtle.Turtle()
    food.shape('circle')
    food.color('red')
    food.penup()
    food.speed(10)
    move_food(food, x_min, x_max, y_min, y_max)

    return food


def move_food(food: turtle, x_min: int, x_max: int, y_min: int, y_max: int):
    x_coord = random.randint(x_min, x_max + 1)
    y_coord = random.randint(y_min, y_max + 1)
    food.goto(x_coord, y_coord)

    return food


def move_snake(snake: turtle):
    (pos_x, pos_y) = snake.pos()

    if snake.direction == 'up':
        snake.goto(pos_x, pos_y + 10)
    elif snake.direction == 'down':
        snake.goto(pos_x, pos_y - 10)
    elif snake.direction == 'right':
        snake.goto(pos_x + 10, pos_y)
    elif snake.direction == 'left':
        snake.goto(pos_x - 10, pos_y)


def grow_snake():
    pass
