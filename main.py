# Simple snake game
#
# @autor Max Jonsson
# @version 11/10-2019

import board
import time

WIDTH, HEIGHT = 600, 600
X_MIN, X_MAX = -290, 280
Y_MIN, Y_MAX = -280, 290

highscore, current_score = 0, 0

window = board.setup_screen(WIDTH, HEIGHT)
scoreboard = board.draw_score_text(0, Y_MAX - 20, current_score, highscore)
snake = board.draw_snake(0, 0)
food = board.span_food(X_MIN, X_MAX, Y_MIN, Y_MAX)


def move_up():
    if snake.direction != 'down':
        snake.direction = 'up'


def move_left():
    if snake.direction != 'right':
        snake.direction = 'left'


def move_right():
    if snake.direction != 'left':
        snake.direction = 'right'


def move_down():
    if snake.direction != 'up':
        snake.direction = 'down'


window.onkey(move_up, 'Up')
window.onkey(move_left, 'Left')
window.onkey(move_right, 'Right')
window.onkey(move_down, 'Down')
window.listen()


def is_collision_wall() -> bool:
    (pos_x, pos_y) = snake.pos()

    return (not X_MIN <= pos_x <= X_MAX) or (not Y_MIN <= pos_y <= Y_MAX)


def is_collision_self() -> bool:
    pass


def is_collision() -> bool:
    return is_collision_wall() or is_collision_self()


def hit_food() -> bool:
    return snake.distance(food) < 20


def grow_mass():
    pass


while True:
    board.move_snake(snake)

    if is_collision():
        current_score = 0
        snake.direction = 'stop'
        board.update_score_text(scoreboard, current_score, highscore)

    if hit_food():
        current_score += 10

        if highscore < current_score:
            highscore = current_score

        grow_mass()
        board.update_score_text(scoreboard, current_score, highscore)
        board.move_food(food, X_MIN, X_MAX, Y_MIN, Y_MAX)

    window.update()
